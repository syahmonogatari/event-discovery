# event discovery web application 🌎📍
> A place to host, share, and gather individuals with common interests in a transparent, digestible platform.  


## Features

**Information at a glance** - digestible map overview of all events in the local area. Users are able to filter on keywords  
**Self-moderated, community driven** - allow users to upvote/downvote/review events. Encourage community interaction  
**Post customization** - Hosts add event information, images, etc. to their event to attract attendees  


## Developer environment setup

### Front end application
1. Complete Angular [tutorial](https://angular.io/tutorial)
2. Install the following software:
    * [Git](https://git-scm.com/)
    * [Visual Studio Code](https://code.visualstudio.com/)
    * [Angular CLI](https://cli.angular.io/)
3. Clone repository to a folder on your computer 
4. Run the following in terminal to start app on local server

```sh
cd event-discovery/APP/event-discovery
ng serve
```

## Release History


* 0.0.0
    * Product Planning Phase
